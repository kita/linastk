from bot import Lina
import constants

import asyncio
import asyncpg
import contextlib
import discord
import logging
from logging.handlers import RotatingFileHandler


class RemoveNoise(logging.Filter):
    def __init__(self):
        super().__init__(name="discord.state")

    def filter(self, record: logging.LogRecord) -> bool:
        if record.levelname == "WARNING" and "referencing an unknown" in record.msg:
            return False
        return True


@contextlib.contextmanager
def setup_logging():
    log = logging.getLogger()

    try:
        dt_fmt = "%Y-%m-%d %H:%M:%S"
        fmt = logging.Formatter(
            "[{asctime}] [{levelname:<7}] {name}: {message} [{pathname}:{lineno}]",
            dt_fmt,
            style="{",
        )

        discord.utils.setup_logging(formatter=fmt)
        # __enter__
        max_bytes = 32 * 1024 * 1024  # 32 MiB
        logging.getLogger("discord").setLevel(logging.INFO)
        logging.getLogger("discord.http").setLevel(logging.WARNING)
        logging.getLogger("discord.state").addFilter(RemoveNoise())

        if hasattr(constants, "DEBUG_LOGS"):
            if constants.DEBUG_LOGS:
                log.setLevel(logging.DEBUG)
            else:
                log.setLevel(logging.INFO)
        else:
            log.setLevel(logging.INFO)

        handler = RotatingFileHandler(
            filename="lina.log",
            encoding="utf-8",
            mode="w",
            maxBytes=max_bytes,
            backupCount=5,
        )
        handler.setFormatter(fmt)
        log.addHandler(handler)

        yield
    finally:
        # __exit__
        handlers = log.handlers[:]
        for hdlr in handlers:
            hdlr.close()
            log.removeHandler(hdlr)


async def createPool() -> asyncpg.Pool:
    return await asyncpg.create_pool(
        constants.POSTGRESQL, command_timeout=300, min_size=20, max_size=20
    )


async def initDatabase(pool: asyncpg.Pool) -> None:
    async with pool.acquire() as con:
        async with con.transaction():

            await con.execute(
                """
            CREATE TABLE IF NOT EXISTS lina_discord_ptrack(
                id bigint NOT NULL PRIMARY KEY,
                usernames text[]
            )
            """
            )
            await con.execute(
                """
            CREATE TABLE IF NOT EXISTS lina_discord_stk_seen(
                username varchar(30) NOT NULL PRIMARY KEY,
                date timestamp without time zone NOT NULL,
                server_name varchar(255) NOT NULL,
                country varchar(2),
                server_country varchar(2)
            )
            """
            )
            await con.execute(
                """
            CREATE TABLE IF NOT EXISTS lina_discord_addons(
                id varchar(255) NOT NULL PRIMARY KEY,
                name varchar(255) NOT NULL,
                file text NOT NULL,
                date int NOT NULL,
                uploader varchar(30),
                designer varchar(255),
                description text,
                image text NOT NULL,
                format int NOT NULL,
                revision int NOT NULL,
                status int NOT NULL,
                size int NOT NULL,
                rating float NOT NULL
            )
            """
            )

            await con.execute(
                """
            CREATE TABLE IF NOT EXISTS lina_discord_stkusers (
                id int NOT NULL PRIMARY KEY,
                username varchar(30) NOT NULL
            )
            """
            )

            await con.execute(
                """
            CREATE TABLE IF NOT EXISTS lina_discord_pokemap (
                id bigint NOT NULL PRIMARY KEY,
                maps text[] NOT NULL,
                cooldown TIMESTAMP WITHOUT TIME ZONE
            )
            """
            )

            await con.execute(
                """
            CREATE TABLE IF NOT EXISTS lina_discord_prefs (
                id bigint NOT NULL PRIMARY KEY,
                lang varchar(6) NOT NULL DEFAULT 'en'
            )
            """
            )


async def runBot():
    log = logging.getLogger("lina")
    try:
        pool = await createPool()
        await initDatabase(pool)
    except Exception:
        log.exception("Could not set up PostgreSQL. Will now exit.")
        return

    async with Lina() as lina:
        lina.pool = pool
        await lina.start()

    log.info("Please wait, just doing some cleaning up...")
    await asyncio.wait_for(pool.close(), 15)

    await asyncio.sleep(5)


if __name__ == "__main__":
    log = logging.getLogger("lina")

    splash = """
    Welcome to linaSTK!

    Copyright (c) 2023-24 searingmoonlight (Ikuyo Kita)

    linaSTK is licensed under the GNU Affero General Public License, version 3.
    There is no warranty. See license for more details. By using this bot, you agree
    to the license terms.

    searingmoonlight will not be responsible with any consequences that you may get
    for using this bot.
    """

    with setup_logging():
        log.info(splash)

        try:
            asyncio.run(runBot())
        except KeyboardInterrupt:
            log.warning(
                "Please don't use CTRL + C to stop Lina. Instead shut it down by executing [p]shutdown."
            )
