import json


def _(s, l):
    base = json.load(open(f"lang/en.json"))

    try:
        data = json.load(open(f"lang/{l}.json"))
    except FileNotFoundError:
        return base.get(s, s)

    return data.get(s, base.get(s, s))
