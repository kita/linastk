from __future__ import annotations

from discord.ext import commands

import constants

from aiohttp import ClientSession
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from bot import Lina


class Gotify(commands.Cog):

    def __init__(self, bot: Lina):
        self.bot: Lina = bot
        self.session: ClientSession = None

    async def cog_load(self):

        self.session = ClientSession(
            constants.GOTIFY_INSTANCE,
            headers={
                "User-Agent": f"linaSTK-Discord/{self.bot.version} (+https://linastk.codeberg.page)"
            },
        )

    async def cog_unload(self):
        if not self.session.closed():
            await self.session.close()

    async def sendNotification(self, *, title: str = None, message: str = None):
        if not constants.ENABLE_GOTIFY:
            return

        if not message:
            raise ValueError("Message required")

        data = {"message": message}

        if title:
            data["title"] = title

        self.bot.loop.create_task(
            self.session.post(f"/message?token={constants.GOTIFY_TOKEN}", data=data)
        )


async def setup(bot: Lina):
    await bot.add_cog(Gotify(bot))
