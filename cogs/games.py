from __future__ import annotations

import discord
from discord import app_commands, ui
from discord.ext import commands

import random
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from bot import Lina


class GuessAddonGame(ui.View):

    def __init__(self, bot: Lina, author_id: int):
        super().__init__(timeout=15)

        self.addonChoices = []
        self.bot: Lina = bot
        self.author_id = author_id
        self.gotCorrectAnswer = False
        self.interaction = None

    async def interaction_check(self, interaction: discord.Interaction):
        if interaction.user.id != self.author_id:
            await interaction.response.send_message("no u", ephemeral=True)
            return False
        return True

    async def on_timeout(self):
        for child in self.children:
            child.disabled = True

        return await self.interaction.edit_original_response(
            embed=discord.Embed(
                title="Times Up!",
                description="You didn't answer on time!",
                color=self.bot.accent_color,
            ),
            view=self,
        )

    async def initGame(self):
        self.correctAddon = random.choice(
            await self.bot.pool.fetch("SELECT id FROM lina_discord_addons")
        )["id"]
        self.correctAddonImage = (
            await self.bot.pool.fetchrow(
                """
                SELECT image FROM lina_discord_addons
                WHERE id = $1
                """,
                self.correctAddon,
            )
        )["image"]

        self.addonChoices.append(
            (
                self.correctAddon,
                (
                    await self.bot.pool.fetchrow(
                        """
                SELECT name FROM lina_discord_addons
                WHERE id = $1
                """,
                        self.correctAddon,
                    )
                )["name"],
                True,
            )
        )

        for i in range(2):
            addon = random.choice(
                await self.bot.pool.fetch("SELECT id FROM lina_discord_addons")
            )["id"]

            self.addonChoices.append(
                (
                    addon,
                    (
                        await self.bot.pool.fetchrow(
                            """
                SELECT name FROM lina_discord_addons
                WHERE id = $1
                """,
                            addon,
                        )
                    )["name"],
                    False,
                )
            )

        random.shuffle(self.addonChoices)

        for c in range(len(self.children)):
            self.children[c].label = self.addonChoices[c][1]

    async def endGame(self, interaction: discord.Interaction):
        for i in range(len(self.children)):
            if self.addonChoices[i][2] is True:
                self.children[i].style = discord.ButtonStyle.green
            self.children[i].disabled = True

        if self.gotCorrectAnswer:
            await interaction.response.edit_message(
                embed=discord.Embed(
                    title="Great Job!",
                    description="You got the correct answer!",
                    color=self.bot.accent_color,
                ),
                view=self,
            )
        else:
            await interaction.response.edit_message(
                embed=discord.Embed(
                    title="Wrong answer!",
                    description="Better luck next time!",
                    color=self.bot.accent_color,
                ),
                view=self,
            )

        self.stop()

    @ui.button(style=discord.ButtonStyle.gray)
    async def choice1(self, interaction: discord.Interaction, button: ui.Button):
        if self.addonChoices[0][2] is True:
            self.gotCorrectAnswer = True
            await self.endGame(interaction)
        else:
            self.choice1.style = discord.ButtonStyle.red
            await self.endGame(interaction)

    @ui.button(style=discord.ButtonStyle.gray)
    async def choice2(self, interaction: discord.Interaction, button: ui.Button):
        if self.addonChoices[1][2] is True:
            self.gotCorrectAnswer = True
            await self.endGame(interaction)
        else:
            self.choice2.style = discord.ButtonStyle.red
            await self.endGame(interaction)

    @ui.button(style=discord.ButtonStyle.gray)
    async def choice3(self, interaction: discord.Interaction, button: ui.Button):
        if self.addonChoices[2][2] is True:
            self.gotCorrectAnswer = True
            await self.endGame(interaction)
        else:
            self.choice3.style = discord.ButtonStyle.red
            await self.endGame(interaction)


class Games(commands.Cog):

    def __init__(self, bot: Lina):
        self.bot: Lina = bot

    @app_commands.command(
        name="guessaddon", description="Guess what addon it is based on the image."
    )
    @app_commands.allowed_contexts(guilds=True, dms=True, private_channels=True)
    @app_commands.allowed_installs(guilds=True, users=True)
    async def guessaddon(self, interaction: discord.Interaction):
        game = GuessAddonGame(self.bot, interaction.user.id)
        await game.initGame()

        await interaction.response.send_message(
            embed=discord.Embed(
                title="Guess what the addon is",
                color=self.bot.accent_color,
            )
            .set_image(url=game.correctAddonImage)
            .set_footer(text="You have 15 seconds to answer."),
            view=game,
            ephemeral=True,
        )
        await game.wait()
        game.interaction = interaction


async def setup(bot: Lina):
    await bot.add_cog(Games(bot))
