from __future__ import annotations

import discord
from discord import app_commands
from discord.ext import commands

from utils import formatting

import datetime
import logging
import random
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from bot import Lina

log = logging.getLogger("lina.cogs.pokemap")


class PokeMap(commands.Cog):

    def __init__(self, bot: Lina):
        self.bot: Lina = bot

    @app_commands.command(name="pokemap")
    async def pokemap(self, interaction: discord.Interaction):

        try:

            cooldown = await self.bot.pool.fetchrow(
                """
                    SELECT cooldown FROM lina_discord_pokemap
                    WHERE id = $1
                    """,
                interaction.user.id,
            )

            if cooldown:

                if (
                    cooldown["cooldown"].timestamp()
                    > datetime.datetime.now().timestamp()
                ):

                    return await interaction.response.send_message(
                        embed=discord.Embed(
                            description="You need to wait **{time}** before catching another pokemap.".format(
                                time=formatting.humanize_timedelta(
                                    timedelta=cooldown["cooldown"]
                                    - datetime.datetime.now()
                                )
                            ),
                            color=self.bot.accent_color,
                        )
                    )

            addon = random.choice(
                await self.bot.pool.fetch("SELECT id FROM lina_discord_addons;")
            )["id"]
            addonInfo = await self.bot.pool.fetchrow(
                """
                SELECT id, name, image FROM lina_discord_addons
                WHERE id = $1
                """,
                addon,
            )

            await self.bot.pool.execute(
                """
                INSERT INTO lina_discord_pokemap
                (id, maps, cooldown)
                VALUES ($1, $2::text[], current_timestamp + '2h' ::interval)
                ON CONFLICT (id) DO UPDATE SET
                maps = array_append(lina_discord_pokemap.maps, $3),
                cooldown = current_timestamp + '2h' ::interval
                """,
                interaction.user.id,
                {addonInfo["id"]},
                addonInfo["id"],
            )
        except Exception:
            log.exception("Could not set pokemap for user %s", interaction.user.id)
            return await interaction.response.send_message(
                embed=discord.Embed(
                    title="Error",
                    description="A database error has occurred. Please contact the developer.",
                    color=self.bot.accent_color,
                ),
                ephemeral=True,
            )

        await interaction.response.send_message(
            embed=discord.Embed(
                description=(
                    "{user}, you've caught a **{addonName}**!\n\n"
                    "`/installaddon {addonId}`"
                ).format(
                    user=interaction.user.mention,
                    addonName=addonInfo["name"],
                    addonId=addonInfo["id"],
                ),
                color=self.bot.accent_color,
            ).set_thumbnail(url=addonInfo["image"])
        )


async def setup(bot: Lina):
    await bot.add_cog(PokeMap(bot))
