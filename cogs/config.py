from __future__ import annotations

import discord
from discord import app_commands
from discord.ext import commands

import json
from typing import TYPE_CHECKING

from l10n import _

if TYPE_CHECKING:
    from bot import Lina


class Config(commands.Cog):

    def __init__(self, bot: Lina):
        self.bot: Lina = bot

    g = app_commands.Group(name="config", description="Configuration")
    lang_G = app_commands.Group(
        name="lang", description="Language Preferences", parent=g
    )

    @lang_G.command(description="For testing localisation")
    async def test(self, i: discord.Interaction):
        lang = self.bot.getPrefLang(i.user.id)

        await i.response.send_message(
            f"""
Current Lang: {lang}
Public Online: {_("online.onlinelist.title", lang)}
Non existent string identifier: {_("non.existent.string", lang)}
"""
        )

    @lang_G.command(name="list", description="List available languages")
    async def listLangs(self, i: discord.Interaction):
        lang = self.bot.getPrefLang(i.user.id)
        data = json.load(open("languages.json"))

        embed = discord.Embed(
            title=_("config.lang.list.title", lang),
            description="",
            color=self.bot.accent_color,
        )

        for l in data:
            cur = data[l]
            hasLocalized = cur["hasLocalizedName"]
            flagCode = cur["flagCode"]
            name = cur["name"]

            if hasLocalized:
                localized = cur["localizedName"]

                embed.description += f"* :flag_{flagCode}: {name} ({localized}) `{l}`\n"
            else:
                embed.description += f"* :flag_{flagCode}: {name} `{l}`\n"

        embed.description += "\n" + str(_("config.lang.promotion", lang)).format(
            link="https://translate.codeberg.org/engage/linastk"
        )

        return await i.response.send_message(embed=embed)

    @lang_G.command(name="set", description="Set your language")
    @app_commands.describe(
        newlang="Language in their code form to switch to. To see available languages, execute '/config lang list'."
    )
    async def setLang(self, i: discord.Interaction, newlang: str):
        await i.response.defer()

        lang = self.bot.getPrefLang(i.user.id)
        langs = json.load(open("languages.json"))

        if newlang not in langs:
            return await i.followup.send(
                embed=discord.Embed(
                    title=_("common.error", lang),
                    description=(_("config.lang.invalid", lang)).format(
                        link="https://translate.codeberg.org/engage/linastk"
                    ),
                    color=self.bot.accent_color,
                )
            )

        self.bot.userLangPrefs[i.user.id] = newlang

        await self.bot.pool.execute(
            """
        INSERT INTO lina_discord_prefs VALUES
        ($1, $2)
        ON CONFLICT (id) DO UPDATE SET lang = $2
        """,
            i.user.id,
            newlang,
        )

        return await i.followup.send(
            embed=discord.Embed(
                title=_("common.success", newlang),
                description=str(_("config.lang.success", newlang)).format(
                    code=langs[newlang]["flagCode"], language=langs[newlang]["name"]
                ),
                color=self.bot.accent_color,
            )
        )


async def setup(bot: Lina):
    await bot.add_cog(Config(bot))
