from __future__ import annotations

import discord
from discord.ext import tasks
from discord import app_commands
from discord.ext import commands

from typing import TYPE_CHECKING
import logging
import urllib.parse

from l10n import _
import constants

log = logging.getLogger("lina.cogs.linker")

if TYPE_CHECKING:
    from bot import Lina


class Linker(commands.Cog):

    def __init__(self, bot: Lina):
        self.bot: Lina = bot
        self.linked_accounts = {}
        self.queue = {}

        self.verifier_userid = None
        self.verifier_token = None

    @tasks.loop(seconds=30)
    async def checker(self):

        data = await self.bot.stkPostReq(
            "/api/v2/user/get-friends-list",
            f"userid={self.bot.stk_userid}&"
            f"token={self.bot.stk_token}&"
            f"visitingid={self.verifier_userid}",
        )

        for i in range(len(data[0])):
            _id = int(data[0][i][0].attrib["id"])
            username = data[0][i][0].attrib["user_name"]

            if _id in self.queue:
                await self.bot.pool.execute(
                    "INSERT INTO lina_discord_linkedaccounts VALUES ($1, $2, $3)",
                    self.queue[_id],
                    _id,
                    username,
                )
                user = self.bot.get_user(self.queue[_id])
                del self.queue[_id]

                log.debug(
                    "Linked account %s (%d) to %s (%d)",
                    username,
                    _id,
                    str(user),
                    user.id,
                )
                await user.send(
                    embed=discord.Embed(
                        title="Successfully linked!",
                        description=f"You have successfully linked your account to {username}",
                        color=self.bot.accent_color,
                    )
                )

    @commands.hybrid_command(name="link", description="Link your SuperTuxKart account")
    @app_commands.describe(id="Your User ID")
    async def link(self, ctx: commands.Context, id: int):

        if ctx.author.id in list(self.queue.values()):
            return await ctx.reply(
                embed=discord.Embed(
                    title="Error",
                    description="You are already linking an account. Cancel the linking process by executing `/cancel-link`",
                    color=self.bot.accent_color,
                )
            )

        current = await self.bot.pool.fetchrow(
            "SELECT * FROM lina_discord_linkedaccounts WHERE id = $1", ctx.author.id
        )

        if current:
            return await ctx.reply(
                embed=discord.Embed(
                    title="Error",
                    description=f"You have already linked your account. The account is: {current['stk_username']} ({current['stk_id']})\n\nTo unlink your account, execute `/unlink`",
                    color=self.bot.accent_color,
                )
            )

        linked = await self.bot.pool.fetchrow(
            "SELECT stk_id FROM lina_discord_linkedaccounts WHERE stk_id = $1", id
        )

        if linked:
            return await ctx.reply(
                embed=discord.Embed(
                    title="Error",
                    description="The user ID you provided has already been linked to a Discord account.",
                    color=self.bot.accent_color,
                )
            )

        self.queue[id] = ctx.author.id

        try:
            await self.bot.stkPostReq(
                "/api/v2/user/friend-request",
                f"userid={self.verifier_userid}&"
                f"token={self.verifier_token}&"
                f"friendid={id}",
            )
        except Exception:
            del self.queue[id]
            return await ctx.reply(
                "Something went wrong trying to send a friend request. Please try again."
            )

        log.debug(
            "Started linking process for user id %d for %s (%d)",
            id,
            str(ctx.author),
            ctx.author.id,
        )

        return await ctx.reply(
            """
I have sent you a friend request. Accept the friend request to link your account.
-# The linking request will expire once the bot restarts. In that case, cancel the friend request and try again."""
        )

    @commands.hybrid_command(
        name="cancel-link",
    )
    async def cancelLink(self, ctx: commands.Context):
        if ctx.author.id in list(self.queue.values()):
            for i in self.queue:
                if self.queue[i] == ctx.author.id:
                    userid = i
                    break

            await self.bot.stkPostReq(
                "/api/v2/user/cancel-friend-request",
                f"userid={self.verifier_userid}&"
                f"token={self.verifier_token}&"
                f"friendid={userid}",
            )

            del self.queue[userid]
            return await ctx.reply("Cancelled linking process.")
        else:
            return await ctx.reply("You are not currently linking an account.")

    @commands.hybrid_command(name="unlink")
    async def unlink(self, ctx: commands.Context):

        current = await self.bot.pool.fetchrow(
            "SELECT * FROM lina_discord_linkedaccounts WHERE id = $1", ctx.author.id
        )

        if not current:
            return await ctx.reply("You are not linked to any account.")

        await self.bot.pool.execute(
            "DELETE FROM lina_discord_linkedaccounts WHERE id = $1", ctx.author.id
        )
        await self.bot.stkPostReq(
            "/api/v2/user/remove-friend",
            f"userid={self.verifier_userid}&"
            f"token={self.verifier_token}&"
            f"friendid={current['stk_id']}",
        )
        log.debug(
            "Unlinked %s (%d) from STK account %s (%d)",
            str(ctx.author),
            ctx.author.id,
            current["stk_username"],
            current["stk_id"],
        )
        return await ctx.reply("Successfully unlinked account.")

    async def cog_load(self):
        data = await self.bot.stkPostReq(
            "/api/v2/user/connect",
            f"username={constants.VERIFIER_USERNAME}"
            f"&password={urllib.parse.quote(constants.VERIFIER_PASSWORD)}",
        )

        log.info("Successfully logged in linker account")

        self.verifier_userid = data.attrib["userid"]
        self.verifier_token = data.attrib["token"]

        linked = await self.bot.pool.fetch("SELECT * FROM lina_discord_linkedaccounts")

        for x in linked:
            self.linked_accounts[x["id"]] = {
                "id": x["stk_id"],
                "username": x["stk_username"],
            }

        log.debug("Loaded linked accounts")
        self.checker.start()

    async def cog_unload(self):
        self.checker.stop()
        await self.bot.stkPostReq(
            "/api/v2/user/client-quit",
            f"userid={self.verifier_userid}&" f"token={self.verifier_token}",
        )


async def setup(bot: Lina):
    await bot.add_cog(Linker(bot))
