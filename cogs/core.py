from __future__ import annotations

import discord
from discord.ext import commands
import logging
from sys import version as python_version
from aiohttp import __version__ as aiohttp_version
from asyncpg import __version__ as asyncpg_version
from typing import TYPE_CHECKING, Optional

log = logging.getLogger("lina.cogs.core")

if TYPE_CHECKING:
    from bot import Lina


class Core(commands.Cog):

    def __init__(self, bot: Lina):
        self.bot: Lina = bot

    @commands.is_owner()
    @commands.command(hidden=True)
    async def load(self, ctx: commands.Context, *, mod: str):
        try:
            await ctx.channel.typing()
            await self.bot.load_extension(mod)
        except commands.ExtensionError as e:
            log.exception("%s: Unable to load:", mod, exc_info=e)
            await ctx.reply(f"{e.__class__.__name__}: {e}")
        else:
            await ctx.reply("Loaded.")

    @commands.is_owner()
    @commands.command(hidden=True)
    async def unload(self, ctx: commands.Context, *, mod: str):
        try:
            await ctx.channel.typing()
            await self.bot.unload_extension(mod)
        except commands.ExtensionError as e:
            log.exception("%s: Unable to unload:", mod, exc_info=e)
            await ctx.reply(f"{e.__class__.__name__}: {e}")
        else:
            await ctx.reply("Unloaded.")

    @commands.is_owner()
    @commands.command(hidden=True)
    async def reload(self, ctx: commands.Context, *, mod: str):
        try:
            await ctx.channel.typing()
            await self.bot.reload_extension(mod)
        except commands.ExtensionError as e:
            log.exception("%s: Unable to reload:", mod, exc_info=e)
            await ctx.reply(f"{e.__class__.__name__}: {e}")
        else:
            await ctx.reply("Reloaded.")

    @commands.group(invoke_without_command=True, hidden=True)
    @commands.is_owner()
    @commands.guild_only()
    async def sync(
        self, ctx: commands.Context, guild_id: Optional[int], copy: bool = False
    ):

        if guild_id:
            guild = discord.Object(guild_id)
        else:
            guild = ctx.guild

        await ctx.channel.typing()
        if copy:
            self.bot.tree.copy_global_to(guild=guild)

        commands = await self.bot.tree.sync(guild=guild)
        await ctx.reply(f"Successfully synced {len(commands)} commands.")

    @sync.command(name="global")
    @commands.is_owner()
    async def sync_global(self, ctx: commands.Context):

        commands = await self.bot.tree.sync()
        await ctx.reply(f"Successfully synced {len(commands)} commands.")

    @commands.is_owner()
    @commands.command(hidden=True)
    async def shutdown(self, ctx: commands.Context):
        await ctx.reply("Shutting down :wave:")
        await self.bot.close()

    @commands.is_owner()
    @commands.command(name="inspect-ptracks", hidden=True)
    async def ptrackInspect(self, ctx: commands.Context, user: discord.User):

        data = await self.bot.pool.fetchrow(
            """
            SELECT usernames FROM lina_discord_ptrack
            WHERE id = $1
            """,
            user.id,
        )

        if not data:
            return await ctx.reply(
                embed=discord.Embed(
                    title="Error",
                    description="This user isn't tracking anyone",
                    color=self.bot.accent_color,
                )
            )
        elif len(data["usernames"]) == 0:
            return await ctx.reply(
                embed=discord.Embed(
                    title="Error",
                    description="This user isn't tracking anyone",
                    color=self.bot.accent_color,
                )
            )
        else:
            return await ctx.author.send(
                embed=discord.Embed(
                    title=f"Players {user} is tracking",
                    description="\n".join([f"* {x}" for x in data["usernames"]]),
                    color=self.bot.accent_color,
                ).set_footer(text=f"Total: {len(data['usernames'])}")
            )

    @commands.hybrid_command(name="version")
    async def version(self, ctx: commands.Context):
        await ctx.reply(
            embed=discord.Embed(
                title="Version information",
                description=(
                    "## Core\n"
                    "**linaSTK**: {version}\n"
                    "**Python**: {pythonversion}\n"
                    "**discord.py**: {discordpyversion}\n"
                    "**AIOHTTP**: {aiohttpversion}\n"
                    "**asyncpg**: {asyncpgversion}\n\n"
                    "## Kernel Version\n"
                    "```\n{kernelver}\n```"
                ).format(
                    version=self.bot.version,
                    discordpyversion=discord.__version__,
                    pythonversion=python_version,
                    kernelver=open("/proc/version").read(),
                    aiohttpversion=aiohttp_version,
                    asyncpgversion=asyncpg_version,
                ),
                color=self.bot.accent_color,
            ).set_thumbnail(url=self.bot.user.avatar.url),
            mention_author=False,
        )


async def setup(bot: Lina):
    await bot.add_cog(Core(bot))
