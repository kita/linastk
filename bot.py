from __future__ import annotations

import discord
from discord import app_commands
from discord.ext import tasks, commands

import logging
import xml.etree.ElementTree as et
import asyncio
from aiohttp import ClientSession
import asyncpg
from itertools import cycle
import urllib.parse
from typing import TYPE_CHECKING, Optional
import time

import constants

log = logging.getLogger("lina.main")
netLog = logging.getLogger("lina.stkaddonsrequest")

if TYPE_CHECKING:
    from cogs import PlayerTrack
    from cogs import Online
    from cogs import Gotify

extensions = (
    "cogs.online",
    "cogs.playertrack",
    "cogs.core",
    "cogs.misc",
    "cogs.pokemap",
    "cogs.games",
    "cogs.config",
    "cogs.linker",
)


class STKRequestError(Exception):
    """Raised when an error occurs upon performing a request to STK servers."""

    pass


class STKAddonsAPIError(Exception):
    pass


class Lina(commands.Bot):
    """
    Class representing lina herself.
    """

    pool: asyncpg.Pool

    def __init__(self):

        intents = discord.Intents.default()
        intents.message_content = True
        intents.members = True

        allowed_mentions = discord.AllowedMentions(everyone=False, roles=False)

        super().__init__(
            intents=intents,
            allowed_mentions=allowed_mentions,
            command_prefix=constants.PREFIX,
        )

        self.accent_color = constants.ACCENT_COLOR
        self.stk_userid: int = None
        self.stk_token: str = None
        self.stk_sessid: str = None
        self.statuses = cycle(
            (
                "{amtStkSeen} players in player tracking database",
                "{online} online players",
                "{stkUsers} STK players in database",
                "{servers} STK Servers",
                "{addons} Addon tracks",
            )
        )
        self.userLangPrefs: dict = {}

        self.version = "1.4rc1"

    def getPrefLang(self, id: int):
        return self.userLangPrefs.get(id, "en")

    async def stkPostReq(self, target, args):
        """Helper function to send a POST request to STK servers."""
        assert self.session is not None

        redactedArgs = args.replace(
            str(self.stk_token), "*" * len(self.stk_token or "")
        ).replace(
            urllib.parse.quote(constants.STK_PASSWORD),
            "*" * len(urllib.parse.quote(constants.STK_PASSWORD)),
        )
        netLog.debug(
            "POST %s -> %s", redactedArgs, str(self.session._base_url) + target
        )

        start = time.perf_counter()
        async with self.session.post(
            target,
            data=args,
            headers={"Content-Type": "application/x-www-form-urlencoded"},
        ) as r:
            r.raise_for_status()

            payload = await r.text()
            data = et.fromstring(payload)

            end = time.perf_counter()

            netLog.debug(
                "%s <- %s?%s [took %d ms]",
                payload.replace("\n", ""),
                str(self.session._base_url) + target,
                redactedArgs,
                (end - start) * 1000,
            )

            if data.attrib["success"] == "no":
                netLog.error(
                    'Got error "%s" trying to request %s?%s',
                    data.attrib["info"],
                    str(self.session._base_url) + target,
                    redactedArgs,
                )
                raise STKRequestError(data.attrib["info"])
            else:
                return data

    async def stkGetReq(self, target):
        """Helper function to send a GET request to STK servers."""
        assert self.session is not None
        async with self.session.get(target) as r:
            r.raise_for_status()
            return et.fromstring(await r.text())

    async def authSTK(self):
        """Authenticate to STK"""
        log.info(f"Trying to authenticate STK account {constants.STK_USERNAME}")
        loginPayload = await self.stkPostReq(
            "/api/v2/user/connect",
            f"username={constants.STK_USERNAME}&"
            f"password={urllib.parse.quote(constants.STK_PASSWORD)}&"
            "save-session=true",
        )

        self.stk_userid = loginPayload.attrib["userid"]
        self.stk_token = loginPayload.attrib["token"]

        log.info(f"STK user {loginPayload.attrib['username']} logged in successfully.")
        if not self.stkPoll.is_running():
            self.stkPoll.start()

    @tasks.loop(minutes=1)
    async def statusChanger(self):
        await self.wait_until_ready()

        status: str = next(self.statuses)

        await self.change_presence(
            activity=discord.CustomActivity(
                name=status.format(
                    amtStkSeen=(
                        await self.pool.fetchrow(
                            "SELECT COUNT(*) FROM lina_discord_stk_seen;"
                        )
                    )["count"],
                    online=len(self.playertrack.onlinePlayers),
                    addons=len(self.online.addons_dict),
                    stkUsers=len(self.online.cachedSTKUsers),
                    servers=len(self.playertrack.serverlist[0]),
                )
            )
        )

    @tasks.loop(minutes=1)
    async def stkPoll(self):
        try:
            await self.stkPostReq(
                "/api/v2/user/poll",
                f"userid={self.stk_userid}&" f"token={self.stk_token}",
            )
        except STKRequestError as e:
            if str(e) in "Session not valid. Please sign in.":
                log.warning("Session was invalidated. Reauthenticating...")
                await self.authSTK()
            else:
                log.error("Poll request failed: %s", e)
        except Exception:
            log.exception("Poll request failed due to exception:")

    @tasks.loop(minutes=10)
    async def stkaddonsSessionPersister(self):
        """
        A very convoluted way of retaining the SuperTuxKart Addons Website's
        STK_SESSID. This is needed to make requests to the frontend API
        """

        log.debug("Trying to obtain STK_SESSID")

        req = await self.session.post(
            "/login.php?action=submit",
            headers={"Content-Type": "application/x-www-form-urlencoded"},
            data=(
                f"username={constants.STK_USERNAME}&"
                f"password={urllib.parse.quote(constants.STK_PASSWORD)}"
            ),
        )

        # A successful login will result in a redirect so check if it happened.
        if not len(req.history) == 1:
            log.warning("Frontend API Login was not successful")
        else:
            self.stk_sessid = req.cookies["STK_SESSID"].value
            log.debug("Successfully obtained STK_SESSID: %s", self.stk_sessid)

    async def on_command_error(
        self, ctx: commands.Context, error: commands.CommandError
    ):
        if isinstance(error, commands.NoPrivateMessage):
            return await ctx.author.send(
                "You can't use this command in private messages."
            )

        if isinstance(error, commands.NotOwner):
            return await ctx.send("This command is restricted to the owner only.")

        if isinstance(error, commands.UserNotFound):
            return await ctx.send(f"User could not be found: {error.argument}")

        if isinstance(error, commands.CommandInvokeError):
            error: commands.CommandInvokeError
            original = error.original

            if isinstance(original, STKRequestError):
                return await ctx.send(
                    embed=discord.Embed(
                        title="Sorry, an STK related error occurred.",
                        description=str(original),
                        color=self.accent_color,
                    )
                )

            return await ctx.send(
                embed=discord.Embed(
                    title="Sorry, this shouldn't have happened. Guru Meditation.",
                    description=f"```\n{error.original.__class__.__name__}: {str(error.original)}\n```",
                    color=self.accent_color,
                )
            )

    async def on_app_command_error(
        self, interaction: discord.Interaction, error: app_commands.AppCommandError
    ):

        if isinstance(error, app_commands.CommandInvokeError):
            error: app_commands.CommandInvokeError
            original = error.original

            if isinstance(original, STKRequestError):
                return await interaction.response.send_message(
                    embed=discord.Embed(
                        title="Sorry, an STK related error occurred.",
                        description=str(original),
                        color=self.accent_color,
                    ),
                    ephemeral=True,
                )

            return await interaction.response.send_message(
                embed=discord.Embed(
                    title="Sorry, this shouldn't have happened. Guru Meditation.",
                    description=f"```\n{error.original.__class__.__name__}: {str(error.original)}\n```",
                    color=self.accent_color,
                ),
                ephemeral=True,
            )

    async def afterStkAuth(self):
        self.stkaddonsSessionPersister.start()
        for extension in extensions:
            log.debug("Loading extension %s", extension)
            try:
                await self.load_extension(extension)
            except Exception:
                log.exception(f"Unable to load extension {extension}.")
            else:
                log.debug("Successfully loaded extension %s.", extension)

    async def setup_hook(self):
        self.tree.error(self.on_app_command_error)

        langData = await self.pool.fetch("SELECT * FROM lina_discord_prefs")

        for i in langData:
            self.userLangPrefs[i["id"]] = i["lang"]

        log.debug("Loaded language preferences.")

        if not hasattr(self, "uptime"):
            self.uptime = discord.utils.utcnow()

        await self.load_extension("cogs.gotify")

        self.session = ClientSession(
            "https://online.supertuxkart.net",
            headers={
                "User-Agent": (
                    f"linaSTK-Discord/{self.version} "
                    "(+https://linastk.codeberg.page/linastk_ua.html)"
                )
            },
        )

        try:
            await self.authSTK()
        except Exception as e:
            log.exception(
                "STK account authentication failed. " "See below for details.",
                exc_info=e,
            )
            await self.close()
        else:
            await self.afterStkAuth()

    async def close(self):
        """Shut down lina"""

        log.info("lina is shutting down...")

        await self.gotify.sendNotification(message="Bot is shutting down.")

        for ext in list(self.extensions):
            try:
                await asyncio.wait_for(self.unload_extension(ext), 15)
            except asyncio.TimeoutError:
                log.warning("Extension %s took too long to unload!!", ext)
            else:
                log.debug("Unloaded %s", ext)

        log.debug("Shutting down tasks")

        self.stkaddonsSessionPersister.stop()
        self.stkPoll.stop()
        self.statusChanger.stop()

        if hasattr(self, "session"):
            # it's important to check if the session is closed in case
            # something went wrong before the session is set up
            if not self.session.closed:
                try:
                    if self.stk_userid and self.stk_token:
                        # send a client quit request so that internal online
                        # counter is deducted and mark the STK account offline
                        await asyncio.wait_for(
                            self.stkPostReq(
                                "/api/v2/user/client-quit",
                                f"userid={self.stk_userid}&" f"token={self.stk_token}",
                            ),
                            timeout=15,
                        )
                        log.debug("sent client quit request")
                    else:
                        log.warning(
                            "userid and token is absent. "
                            "will not send client quit request."
                        )
                finally:
                    await asyncio.wait_for(self.session.close(), 10)
                    log.debug("session has been closed")

        await asyncio.wait_for(super().close(), 15)

    async def start(self):
        """Bring lina to life"""
        log.info("Starting bot...")
        await super().start(constants.TOKEN, reconnect=True)

    @property
    def playertrack(self) -> Optional[PlayerTrack]:
        """Represents the PlayerTrack cog"""
        return self.get_cog("PlayerTrack")

    @property
    def online(self) -> Optional[Online]:
        """Represents the Online cog"""
        return self.get_cog("Online")

    @property
    def gotify(self) -> Optional[Gotify]:
        return self.get_cog("Gotify")

    async def on_ready(self):
        self.statusChanger.start()

        log.info(f"Bot {self.user} ({self.user.id}) is ready!")
        await self.gotify.sendNotification(message="Bot started!")
